import { PortraitConfig } from "./module/portrait-config.js";
import { SidebarAestheticsConfig } from "./module/config.js";

/**
 * Registers Settings
 */
function registerSettings() {

  /**
   * Chat portrait settings
   */
  game.settings.registerMenu("sidebar-aesthetics", "portraitConfig", {
    name: "SETTINGS.saRollPortraitN",
    label: "SETTINGS.saRollPortraitLabel",
    hint: "SETTINGS.saRollPortraitH",
    type: PortraitConfig,
    restricted: false,
  });
  game.settings.register("sidebar-aesthetics", "portraitConfig", {
    name: "SETTINGS.saRollPortraitN",
    scope: "client",
    config: false,
    default: PortraitConfig.defaultSettings,
    type: Object,
    onChange: () => window.location.reload(),
  });

  /**
   * Show journal thumbnails
   */
  game.settings.register("sidebar-aesthetics", "showJournalThumbnails", {
    name: "SETTINGS.saShowJournalImagesN",
    hint: "SETTINGS.saShowJournalImagesH",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: () => window.location.reload(),
  });

  /**
   * Show actor tokens instead of their portraits on the sidebar
   */
  game.settings.register("sidebar-aesthetics", "showActorTokens", {
    name: "SETTINGS.saShowActorTokensN",
    hint: "SETTINGS.saShowActorTokensH",
    scope: "client",
    config: true,
    default: false,
    type: Boolean,
    onChange: () => window.location.reload(),
  });
}

function renderJournalThumbnails(app, html, data) {
  app.entities.forEach(o => {
    if (!o.data.img) return;
    const htmlEntry = html.find(`.directory-item.entity[data-entity-id="${o.id}"]`);
    if (htmlEntry.length !== 1) return;

    htmlEntry.prepend(`<img class="sidebar-image" src="${o.data.img}" title="${o.name}" class="journal-entry-image">`);
  });
}

function renderActorTokens(app, html, data) {
  app.entities.forEach(o => {
    let tokenImg = getProperty(o.data, "token.img");
    if (!tokenImg) return;
    if (getProperty(o.data, "token.randomImg") === true) {
      tokenImg = tokenImg.replace("*", "1");
    }

    const htmlEntry = html.find(`.directory-item.entity[data-entity-id="${o.id}"]`)[0];
    if (htmlEntry == null) return;

    const htmlImage = $(htmlEntry).find("img.profile")[0];
    if (htmlImage.dataset.src != null) htmlImage.dataset.src = tokenImg;
    else htmlImage.src = tokenImg;
  });
}

function renderChatPortraits(type, app, html, data) {
  const speaker = getProperty(app, "data.speaker");
  const portraitConfig = game.settings.get("sidebar-aesthetics", "portraitConfig");
  if (!speaker) return;
  if (!portraitConfig.showOn.rawMessages && isRawMessage(html)) return;
  if (!portraitConfig.showOn.contextMessages && !isRawMessage(html)) return;

  let actor = null,
    actorId = speaker.actor,
    tokenId = speaker.token;
  const imgSize = getPortraitSize(app);

  actor = game.actors.tokens[tokenId];
  if (!actor) actor = game.actors.get(actorId);
  if (actor == null) return;

  let img = actor.data.img;
  if (type === "tokens") {
    if (getProperty(actor, "data.token.img") != null) img = getProperty(actor, "data.token.img");
  }

  // Handle contextual message
  if (!isRawMessage(html)) {
    html.prepend(`<div class="message-header-parent flexrow"><div class="flexcol"></div></div>`);

    const header = html.find(".message-header");
    const headerParent = html.find(".message-header-parent");
    headerParent.prepend(`<img class="message-portrait" src="${img}" style="max-width: ${imgSize}px; max-height: ${imgSize}px;">`);
    headerParent.find("> .flexcol").append(header);
  }
  // Handle raw messages
  else {
    html.prepend(`<div class="message-header-parent"></div>`);
    html.find(".message-header-parent").append(`<img class="message-portrait" src="${img}" style="max-width: ${imgSize}px; max-height: ${imgSize}px;">`);
    html.find(".message-header-parent").append(html.find(".message-header"));
    html.find(".message-header-parent").append(html.find(".message-content"));
  }

  // Add border
  const imgElem = html.find("img.message-portrait");
  if (portraitConfig.borderColor.length) {
    const user = getChatMessageAuthor(app);

    let borderColor = portraitConfig.borderColor;
    if (borderColor === "player" && user) borderColor = user.data.color;

    let borderSize = portraitConfig.borderSize;

    imgElem.css("border", `${borderSize}px solid ${borderColor}`);
    if (borderSize > 1) {
      imgElem.css("border-style", "outset");
    }
  }
}

function getChatMessageAuthor(chatMessage) {
  if (!chatMessage instanceof ChatMessage) return null;

  const userId = chatMessage.data.user;
  return game.users.find(o => o._id === userId);
}

function getPortraitSize() {
  return CONFIG.SidebarAesthetics.chatPortraitSizes[game.settings.get("sidebar-aesthetics", "portraitConfig").size].size;
}

function isRawMessage(html) {
  const message = html.find(".message-content");
  if (!message.length) return true;
  let children = message.children().filter((a, elem) => {
    if (["BR"].includes(elem.tagName)) return false;
    return true;
  });
  return (children.length === 0);
}

function updateTokenImage(actor, img) {
  if (!game.settings.get("sidebar-aesthetics", "showActorTokens")) return;

  const imgElem = $(`li[data-entity-id="${actor._id}"] img`)[0];
  if (!imgElem) return;

  if (imgElem.src) {
    delete imgElem.dataset.src;
    imgElem.src = img;
  }
  else imgElem.dataset.src = img;
}

Hooks.once("init", async function() {
  CONFIG.SidebarAesthetics = SidebarAestheticsConfig;
  registerSettings();
});

// Translate config strings
Hooks.once("setup", function() {
  for (let o of Object.values(CONFIG.SidebarAesthetics.chatPortraitSizes)) {
    o.label = game.i18n.localize(o.label);
  }
});

Hooks.on("renderJournalDirectory", function(app, html, data) {
  if (game.settings.get("sidebar-aesthetics", "showJournalThumbnails")) {
    renderJournalThumbnails(app, html, data);
  }
});

Hooks.on("renderActorDirectory", function(app, html, data) {
  if (game.settings.get("sidebar-aesthetics", "showActorTokens")) {
    renderActorTokens(app, html, data);
  }
});

Hooks.on("updateActor", (actor, updateData, options, user) => {
  if (hasProperty(updateData, "token.img")) {
    updateTokenImage(actor, updateData.token.img);
  }
});

Hooks.on("renderChatMessage", (app, html, data) => {
  const renderPortraits = game.settings.get("sidebar-aesthetics", "portraitConfig").portraitType;
  renderChatPortraits(renderPortraits, app, html, data);
});